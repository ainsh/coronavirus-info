package com.ainur.coronavirus.info.config;

import com.ainur.coronavirus.info.service.CountryCaseLoadService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class AppStartupRunner implements ApplicationRunner {

    private static final Logger LOG = LoggerFactory.getLogger(AppStartupRunner.class);

    private final CountryCaseLoadService countryCaseLoadService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        LOG.info("@@@@ Data download started. @@@@");

        countryCaseLoadService.initialLoadCountries();
        countryCaseLoadService.initialLoadCountriesConfirmedCases();

        LOG.info("@@@@ Data download completed. @@@@");
    }
}
