package com.ainur.coronavirus.info;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoronavirusInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoronavirusInfoApplication.class, args);
	}

}
