package com.ainur.coronavirus.info.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
public class CountryCaseDto {

    private Integer id;
    private String countryName;
    private Integer cases;
    private LocalDate date;

}
