package com.ainur.coronavirus.info.model.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Country {
    @Schema(description = "Country name")
    private String country;

    @Schema(description = "Country web name")
    private String slug;

    @Schema(description = "Abbreviated name of the country")
    private String iso2;
}
