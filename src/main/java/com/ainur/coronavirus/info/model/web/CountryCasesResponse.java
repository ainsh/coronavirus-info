package com.ainur.coronavirus.info.model.web;

import com.ainur.coronavirus.info.model.domain.CountryCase;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryCasesResponse {

    @Schema(description = "cases for the countries")
    private List<CountryCasesReport> countriesCases = new ArrayList<>();

}
