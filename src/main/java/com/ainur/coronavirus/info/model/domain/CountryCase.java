package com.ainur.coronavirus.info.model.domain;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryCase {

    @Schema(description = "Country name")
    private String country;

    @Schema(description = "Country case")
    private Integer cases;

    @Schema(description = "Country web name")
    private LocalDate date;

}
