package com.ainur.coronavirus.info.model.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryCaseResponse implements Serializable {
    @JsonProperty("Country")
    private String country;

    @JsonProperty("Cases")
    private Integer cases;

    @JsonProperty("Date")
    private LocalDate date;
}
