package com.ainur.coronavirus.info.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CountryDto {

    private Integer id;
    private String country;
    private String slug;
    private String iso2;

}
