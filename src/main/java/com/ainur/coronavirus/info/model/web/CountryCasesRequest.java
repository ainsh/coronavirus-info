package com.ainur.coronavirus.info.model.web;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryCasesRequest {

    @NotNull(message = "Must not be null")
    @Size(min = 1, message = "minimum array size is 1")
    @Schema(description = "Array of full country names for data retrieving")
    private List<@NotBlank String> countries;

    @NotNull(message = "field must not be null")
    @Schema(description = "Initial date of data extraction")
    private LocalDate from;

    @NotNull(message = "field must not be null")
    @Schema(description = "Last date of data extraction")
    private LocalDate to;
}
