package com.ainur.coronavirus.info.controller;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class HomeController {

    private static final Logger LOG = LoggerFactory.getLogger(HomeController.class);

    @Operation(summary = "Get app-name, app-version.")
    @GetMapping("/")
    public Map<String, String> getStatus() {
        LOG.info("----Home Controller----");
        Map<String, String> map = new HashMap<>();
        map.put("app-name", "coronavirus-info");
        map.put("app-version", "1.0.0");
        return map;
    }
}
