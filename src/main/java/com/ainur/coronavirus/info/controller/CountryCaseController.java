package com.ainur.coronavirus.info.controller;

import com.ainur.coronavirus.info.model.web.CountryCasesRequest;
import com.ainur.coronavirus.info.model.web.CountryCasesReport;
import com.ainur.coronavirus.info.model.web.CountryCasesResponse;
import com.ainur.coronavirus.info.service.CountryCaseService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryCaseController {

    private static final Logger LOG = LoggerFactory.getLogger(CountryCaseController.class);

    private final CountryCaseService countryCaseService;

    @Operation(summary = "Get country case list.")
    @PostMapping ("/country/case/get")
    public ResponseEntity<CountryCasesResponse> getCountryCases(@RequestBody @Valid CountryCasesRequest request) {
        LOG.info("Entering getCountryCases method with request ::: {}", request.toString());
        CountryCasesResponse countriesCasesResponse = countryCaseService.getCountryCases(request);
        return ResponseEntity.ok(countriesCasesResponse);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

}
