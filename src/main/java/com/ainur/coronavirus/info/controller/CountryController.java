package com.ainur.coronavirus.info.controller;

import com.ainur.coronavirus.info.mapper.CountryMapper;
import com.ainur.coronavirus.info.model.domain.Country;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.service.CountryService;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryController {

    private static final Logger LOG = LoggerFactory.getLogger(CountryController.class);

    private final CountryService countryService;
    private final CountryMapper countryMapper;

    @Operation(summary = "Get country list.")
    @GetMapping("/country/all")
    public ResponseEntity<List<Country>> getAllCountries() {
        LOG.info("Entering getAllCountries method.");

        ResponseEntity responseEntity = null;
        List<CountryDto> countryDtoList = countryService.getAll();
        if (countryDtoList != null) {
            List<Country> countries = countryDtoList.stream().map(countryMapper::toDomain).collect(Collectors.toList());
            responseEntity = new ResponseEntity(countries, HttpStatus.OK);
        } else {
            responseEntity = new ResponseEntity(new ArrayList<>(), HttpStatus.OK);
        }

        return responseEntity;
    }

}
