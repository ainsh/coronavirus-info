package com.ainur.coronavirus.info.mapper;

import com.ainur.coronavirus.info.model.domain.Country;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.model.web.CountryResponse;
import org.springframework.stereotype.Component;

@Component
public class CountryMapper {

    public CountryDto toDto(CountryResponse cr) {
        CountryDto countryDto = new CountryDto();
        countryDto.setCountry(cr.getCountry());
        countryDto.setSlug(cr.getSlug());
        countryDto.setIso2(cr.getIso2());
        return countryDto;
    }

    public Country toDomain(CountryDto dto){
        Country country = new Country();
        country.setCountry(dto.getCountry());
        country.setSlug(dto.getSlug());
        country.setIso2(dto.getIso2());

        return country;
    }
}
