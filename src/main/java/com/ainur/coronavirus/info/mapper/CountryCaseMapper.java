package com.ainur.coronavirus.info.mapper;

import com.ainur.coronavirus.info.model.domain.CountryCase;
import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.web.CountryCaseResponse;
import org.springframework.stereotype.Component;

@Component
public class CountryCaseMapper {

    public CountryCaseDto toDto(CountryCaseResponse ccr) {
        CountryCaseDto countryCaseDto = new CountryCaseDto();
        countryCaseDto.setCountryName(ccr.getCountry());
        countryCaseDto.setCases(ccr.getCases());
        countryCaseDto.setDate(ccr.getDate());
        return countryCaseDto;
    }

    public CountryCase toDomain(CountryCaseDto dto) {
        CountryCase countryCase = new CountryCase();
        countryCase.setCountry(dto.getCountryName());
        countryCase.setCases(dto.getCases());
        countryCase.setDate(dto.getDate());

        return countryCase;
    }

}
