package com.ainur.coronavirus.info.service;

import com.ainur.coronavirus.info.model.web.CountryCaseResponse;
import com.ainur.coronavirus.info.model.web.CountryResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public interface CovidApiService {

    ResponseEntity<CountryResponse[]> getCountries();

    ResponseEntity<CountryCaseResponse[]> getCountryConfirmedCases(String countrySlug, LocalDate fromDate, LocalDate toDate);

}
