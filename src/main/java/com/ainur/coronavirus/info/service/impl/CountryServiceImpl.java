package com.ainur.coronavirus.info.service.impl;

import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.repository.CountryRepository;
import com.ainur.coronavirus.info.service.CountryService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryServiceImpl implements CountryService {

    private static final Logger LOG = LoggerFactory.getLogger(CountryCaseLoadServiceImpl.class);

    private final CountryRepository countryRepository;

    @Override
    public void saveAll(List<CountryDto> countryDtoList) {
        LOG.info("Started saving countryList to DB.");
        int[] resultArr = countryRepository.saveAll(countryDtoList);
        if (resultArr.length == countryDtoList.size()) {
            LOG.info("All {} countries were saved to DB.", resultArr.length);
        } else {
            LOG.error("{} countries were not saved to DB.", (countryDtoList.size() -  resultArr.length));
        }

    }

    @Override
    public List<CountryDto> getAll() {
        LOG.info("Started retrieving countryList from DB.");
        List<CountryDto> countryDtoList = countryRepository.getAll();

        if (countryDtoList != null) {
            LOG.info("Retrieving countryList from DB is finished. countryList size: {}", countryDtoList.size());
        } else {
            LOG.error("Retrieving countryList from DB is failed. countryList is null");
        }
        return countryDtoList;
    }
}
