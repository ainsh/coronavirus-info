package com.ainur.coronavirus.info.service;

import com.ainur.coronavirus.info.model.dto.CountryDto;

import java.util.List;

public interface CountryService {

    void saveAll(List<CountryDto> countryDtoList);

    List<CountryDto> getAll();

}
