package com.ainur.coronavirus.info.service.impl;

import com.ainur.coronavirus.info.mapper.CountryCaseMapper;
import com.ainur.coronavirus.info.mapper.CountryMapper;
import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.model.web.CountryCaseResponse;
import com.ainur.coronavirus.info.model.web.CountryResponse;
import com.ainur.coronavirus.info.service.CountryCaseService;
import com.ainur.coronavirus.info.service.CountryCaseLoadService;
import com.ainur.coronavirus.info.service.CountryService;
import com.ainur.coronavirus.info.service.CovidApiService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryCaseLoadServiceImpl implements CountryCaseLoadService {

    private static final Logger LOG = LoggerFactory.getLogger(CountryCaseLoadServiceImpl.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private static final String ZONE_ID = "Z";

    private final CountryService countryService;

    private final CountryCaseService countryCaseService;

    private final CovidApiService covidApiService;

    private final CountryMapper countryMapper;

    private final CountryCaseMapper countryCaseMapper;

    @Value("${coronavirus-info.country.cases.confirmed.load-from}")
    private String casesConfirmedLoadFromDate;

    @Value("#{${coronavirus-info.country.list}}")
    private List<String> countryList;

    @Value("${coronavirus-info.country.filter}")
    private Boolean countryFilter;

    @Value("${coronavirus-info.country.loadSleepTime}")
    private Long loadSleepTime;

    @Override
    public void initialLoadCountries() {
        LOG.info("Started loading countries.");

        ResponseEntity<CountryResponse[]> responseEntity = covidApiService.getCountries();

        if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) {
            CountryResponse[] countryResponseArr = responseEntity.getBody();
            if (countryResponseArr != null && countryResponseArr.length > 0) {
                List<CountryDto> countryDtoList = Arrays.stream(countryResponseArr)
                        .filter(country -> !countryFilter || countryList.contains(country.getCountry()))
                        .map(countryMapper::toDto)
                        .collect(Collectors.toList());
                countryService.saveAll(countryDtoList);
            } else {
                LOG.error("Failed loading countries. Country Response Array is empty or null");
            }
        } else {
            LOG.error("Failed loading countries");
        }
    }

    @Override
    public void initialLoadCountriesConfirmedCases() {
        LOG.info("Started initial loading countries confirmed cases.");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        LocalDate fromDateDefault = LocalDate.parse(casesConfirmedLoadFromDate, formatter);
        LocalDate toDateParam = LocalDate.now(ZoneId.of(ZONE_ID));

        loadCountriesConfirmedCases(fromDateDefault, toDateParam);
    }

    private void loadCountriesConfirmedCases(LocalDate fromDateDefault, LocalDate toDate) {
        List<CountryDto> countryDtoList = countryService.getAll();
        List<CountryCaseDto> lastCases = countryCaseService.getLastCases(countryDtoList);
        Map<String, CountryCaseDto> countryLastCaseDtoMap = lastCases.stream().collect(Collectors.toMap(CountryCaseDto::getCountryName, Function.identity()));

        if (countryDtoList != null && countryDtoList.size() > 0) {
            for (CountryDto countryDto : countryDtoList) {
                LocalDate fromDate = null;
                if (countryLastCaseDtoMap.containsKey(countryDto.getCountry())) {
                    CountryCaseDto countryCaseDto = countryLastCaseDtoMap.get(countryDto.getCountry());
                    fromDate = countryCaseDto.getDate();

                    if (fromDate.equals(toDate)) {
                        LOG.debug("Country case load for \"{}\" will be skipped", countryDto.getCountry());
                        continue;
                    }
                } else {
                    fromDate = fromDateDefault;
                }

                //loading is paused to avoid errors from Covid API
                try {
                    Thread.sleep(loadSleepTime);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

                ResponseEntity<CountryCaseResponse[]> responseEntity = covidApiService.getCountryConfirmedCases(countryDto.getSlug(), fromDate, toDate);
                if (responseEntity != null && responseEntity.getStatusCode().is2xxSuccessful()) {
                    CountryCaseResponse[] countryCasesArr = responseEntity.getBody();

                    List<CountryCaseDto> countryCaseDtoList = null;
                    //filtering last case
                    if (countryLastCaseDtoMap.containsKey(countryDto.getCountry())) {
                        CountryCaseDto countryCaseDto = countryLastCaseDtoMap.get(countryDto.getCountry());
                        countryCaseDtoList = Arrays.stream(countryCasesArr)
                                .filter(cc -> !cc.getDate().equals(countryCaseDto.getDate()))
                                .map(countryCaseMapper::toDto)
                                .collect(Collectors.toList());
                    } else {
                        countryCaseDtoList = Arrays.stream(countryCasesArr)
                                .map(countryCaseMapper::toDto)
                                .collect(Collectors.toList());
                    }

                    countryCaseService.saveAll(countryCaseDtoList, countryDto);
                }
            }
        }
    }

    @Override
    public void updateConfirmedCases() {
        LOG.info("Started updating countries confirmed cases.");

        LocalDate fromDateDefault = LocalDate.now(ZoneId.of(ZONE_ID)).minusDays(1);
        LocalDate toDateParam = LocalDate.now(ZoneId.of(ZONE_ID));

        loadCountriesConfirmedCases(fromDateDefault, toDateParam);

        LOG.info("Updating countries confirmed cases is finished.");
    }

}
