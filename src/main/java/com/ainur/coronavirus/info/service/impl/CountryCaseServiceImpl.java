package com.ainur.coronavirus.info.service.impl;

import com.ainur.coronavirus.info.mapper.CountryCaseMapper;
import com.ainur.coronavirus.info.model.domain.CountryCase;
import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.model.web.CountryCasesReport;
import com.ainur.coronavirus.info.model.web.CountryCasesRequest;
import com.ainur.coronavirus.info.model.web.CountryCasesResponse;
import com.ainur.coronavirus.info.repository.CountryCaseRepository;
import com.ainur.coronavirus.info.service.CountryCaseService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryCaseServiceImpl implements CountryCaseService {

    private static final Logger LOG = LoggerFactory.getLogger(CountryCaseServiceImpl.class);

    private final CountryCaseRepository countryCaseRepository;

    private final CountryCaseMapper countryCaseMapper;

    @Value("#{${coronavirus-info.country.list}}")
    private List<String> countryList;

    @Value("${coronavirus-info.country.filter}")
    private Boolean countryFilter;

    @Override
    public void saveAll(List<CountryCaseDto> countryCaseDtoList, CountryDto countryDto) {
        LOG.info("Started saving countryCaseList for country=\"{}\" to DB.", countryDto.getCountry());
        if (countryCaseDtoList != null && countryCaseDtoList.size() > 0) {
            int[] resultArr = countryCaseRepository.saveAll(countryCaseDtoList, countryDto);
            if (resultArr.length == countryCaseDtoList.size()) {
                LOG.info("All {} countryCases were saved to DB.", resultArr.length);
            } else {
                LOG.error("{} countryCases were not saved to DB.", (countryCaseDtoList.size() - resultArr.length));
            }
        } else {
            LOG.info("countryCaseList for country=\"{}\" is empty!", countryDto.getCountry());
        }
    }

    @Override
    public List<CountryCaseDto> getLastCases(List<CountryDto> countryDtoList) {
        List<Integer> countryIdList = countryDtoList.stream()
                .filter(country -> !countryFilter || countryList.contains(country.getCountry()))
                .map(CountryDto::getId)
                .collect(Collectors.toList());

        List<CountryCaseDto> lastCountryCaseDtos = countryCaseRepository.getLastCasesByCountryIds(countryIdList);

        return lastCountryCaseDtos;
    }

    @Override
    public CountryCasesResponse getCountryCases(CountryCasesRequest request) {
        LOG.info("Started retrieving countries confirmed cases from DB.");
        CountryCasesResponse countryCasesResponse = new CountryCasesResponse();
        List<CountryCaseDto> cases = countryCaseRepository.getCountryCasesByDates(request.getCountries(), request.getFrom(), request.getTo());
        if (cases != null && cases.size() > 0) {
            Map<String, List<CountryCase>> casesMap = cases.stream()
                    .map(countryCaseMapper::toDomain)
                    .collect(Collectors.groupingBy(CountryCase::getCountry));

            for (Map.Entry<String, List<CountryCase>> entry : casesMap.entrySet()) {
                String countryName = entry.getKey();
                List<CountryCase> countryCases = entry.getValue();
                CountryCasesReport report = new CountryCasesReport(countryName, countryCases);
                countryCasesResponse.getCountriesCases().add(report);
            }

        } else {
            LOG.error("Retrieving countries confirmed is failed, no data from DB.");
        }

        LOG.info("Retrieving countries confirmed cases from DB is finished.");
        return countryCasesResponse;
    }
}
