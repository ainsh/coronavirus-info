package com.ainur.coronavirus.info.service;

public interface CountryCaseLoadService {

    void initialLoadCountries();

    void initialLoadCountriesConfirmedCases();

    void updateConfirmedCases();
}
