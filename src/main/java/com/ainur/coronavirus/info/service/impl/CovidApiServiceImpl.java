package com.ainur.coronavirus.info.service.impl;

import com.ainur.coronavirus.info.model.web.CountryCaseResponse;
import com.ainur.coronavirus.info.model.web.CountryResponse;
import com.ainur.coronavirus.info.service.CovidApiService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CovidApiServiceImpl implements CovidApiService {

    private static final Logger LOG = LoggerFactory.getLogger(CovidApiServiceImpl.class);

    private static final String DATE_FORMAT = "yyyy-MM-dd";
    private static final String DATE_ENDING = "T00:00:00Z";

    private final RestTemplate restTemplate;

    @Value("${coronavirus-info.user}")
    private String userName;

    @Value("${coronavirus-info.password}")
    private String password;
    @Value("${coronavirus-info.url.countries}")
    private String urlCountries;

    @Value("${coronavirus-info.url.country.cases.confirmed}")
    private String urlCountryCasesConfirmed;

    @Override
    public ResponseEntity<CountryResponse[]> getCountries() {
        ResponseEntity<CountryResponse[]> responseEntity = null;
        LOG.debug("Call Loading countries API.");
        try {
            HttpEntity reqEntity = new HttpEntity(getHeader());
            responseEntity = restTemplate.getForEntity(urlCountries, CountryResponse[].class, reqEntity);
            LOG.debug("Loading countries response status is {} {}.", responseEntity.getStatusCode().value(), responseEntity.getStatusCode().getReasonPhrase());
        } catch (Exception e)  {
            LOG.error("Failed loading countries", e);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<CountryCaseResponse[]> getCountryConfirmedCases(String countrySlug, LocalDate fromDate, LocalDate toDate) {
        ResponseEntity<CountryCaseResponse[]> responseEntity = null;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("country", countrySlug);
        uriVariables.put("fromDate", formatter.format(fromDate) + DATE_ENDING);
        uriVariables.put("toDate", formatter.format(toDate) + DATE_ENDING);

        LOG.debug("Call Loading country confirmed cases API with params: {}", uriVariables);

        try {
            HttpEntity reqEntity = new HttpEntity(getHeader());
            responseEntity = restTemplate.exchange(
                    urlCountryCasesConfirmed,
                    HttpMethod.GET,
                    reqEntity,
                    CountryCaseResponse[].class,
                    uriVariables
            );
            LOG.debug("Loading country confirmed cases for \"{}\" response status is {} {}.",
                    countrySlug,
                    responseEntity.getStatusCode().value(), responseEntity.getStatusCode().getReasonPhrase());
        } catch (Exception e) {
            LOG.error("Failed loading country confirmed cases for \"{}\"", countrySlug, e);
        }

        return responseEntity;
    }

    private HttpHeaders getHeader(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setBasicAuth(userName, password);
        return headers;
    }

}
