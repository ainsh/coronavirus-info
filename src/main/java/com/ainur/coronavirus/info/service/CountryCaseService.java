package com.ainur.coronavirus.info.service;

import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import com.ainur.coronavirus.info.model.web.CountryCasesRequest;
import com.ainur.coronavirus.info.model.web.CountryCasesResponse;

import java.util.List;

public interface CountryCaseService {

    void saveAll(List<CountryCaseDto> countryCaseDtoList, CountryDto countryDto);

    List<CountryCaseDto> getLastCases(List<CountryDto> countryDtoList);

    CountryCasesResponse getCountryCases(CountryCasesRequest request);
}
