package com.ainur.coronavirus.info.service.impl;

import com.ainur.coronavirus.info.service.CountryCaseLoadService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryCaseSchedulerServiceImpl {

    private static final Logger LOG = LoggerFactory.getLogger(CountryCaseSchedulerServiceImpl.class);

    private final CountryCaseLoadService countryCaseLoadService;

    @Scheduled(fixedRateString = "#{${coronavirus-info.country.update.time} * 1000 * 60}",
            initialDelayString = "#{${coronavirus-info.country.update.time} * 1000 * 60}")
    public void scheduleFixedRateTask() {
        countryCaseLoadService.updateConfirmedCases();
    }
}
