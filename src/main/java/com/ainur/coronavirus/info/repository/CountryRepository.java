package com.ainur.coronavirus.info.repository;

import com.ainur.coronavirus.info.model.dto.CountryDto;

import java.util.List;

public interface CountryRepository {

    int[] saveAll(List<CountryDto> countryDtoList);

    List<CountryDto> getAll();

}
