package com.ainur.coronavirus.info.repository;

import com.ainur.coronavirus.info.model.dto.CountryDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryRepositoryImpl implements CountryRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    @Transactional
    public int[] saveAll(List<CountryDto> countryDtoList) {
        final String query = "INSERT INTO country (country_name, slug, iso2) VALUES (:country_name, :slug, :iso2)";

        List<MapSqlParameterSource> paramList = countryDtoList.stream().map(country -> {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("country_name", country.getCountry());
            params.addValue("slug", country.getSlug());
            params.addValue("iso2", country.getIso2());
            return params;
        }).collect(Collectors.toList());
        MapSqlParameterSource[] mapSqlParameterSources = paramList.toArray(MapSqlParameterSource[]::new);

        int[] ints = jdbcTemplate.batchUpdate(query, mapSqlParameterSources);

        return ints;
    }

    @Override
    public List<CountryDto> getAll() {
        final String query = "SELECT * FROM country";
        List<CountryDto> countryDtoList = jdbcTemplate.query(
                query,
                (rs, rowNum) -> {
                    CountryDto countryDto = new CountryDto();
                    countryDto.setId(rs.getInt("id"));
                    countryDto.setCountry(rs.getString("country_name"));
                    countryDto.setSlug(rs.getString("slug"));
                    countryDto.setIso2(rs.getString("iso2"));
                    return countryDto;
                });

        return countryDtoList;
    }
}
