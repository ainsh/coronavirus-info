package com.ainur.coronavirus.info.repository;

import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.dto.CountryDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CountryCaseRepositoryImpl implements CountryCaseRepository {

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    private final NamedParameterJdbcTemplate jdbcTemplate;

    @Override
    public int[] saveAll(List<CountryCaseDto> countryCaseDtoList, CountryDto countryDto) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        final String query = "INSERT INTO country_case (country_id, cases, date) VALUES (:country_id, :cases, :date)";
        List<MapSqlParameterSource> paramList = countryCaseDtoList.stream().map(countryCase -> {
            MapSqlParameterSource params = new MapSqlParameterSource();
            params.addValue("country_id", countryDto.getId());
            params.addValue("cases", countryCase.getCases());
            params.addValue("date", formatter.format(countryCase.getDate()));
            return params;
        }).collect(Collectors.toList());
        MapSqlParameterSource[] mapSqlParameterSources = paramList.toArray(MapSqlParameterSource[]::new);

        int[] ints = jdbcTemplate.batchUpdate(query, mapSqlParameterSources);

        return ints;
    }

    @Override
    public List<CountryCaseDto> getLastCasesByCountryIds(List<Integer> countryIds) {
        final String query = "SELECT t.id as id, c.country_name as country_name, t.cases as cases, t.date as date FROM  " +
                "(SELECT id, country_id, cases, date, row_number() OVER " +
                "(PARTITION BY country_id ORDER BY date DESC) as rn from country_case WHERE country_id in (:countryIds)) as t " +
                "INNER JOIN country as c on c.id=t.country_id WHERE t.rn = 1;";
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("countryIds", countryIds);

        List<CountryCaseDto> countryCaseDtoList = jdbcTemplate.query(query, params,
                (rs, rowNum) -> {
                    CountryCaseDto countryCaseDto = new CountryCaseDto();
                    countryCaseDto.setId(rs.getInt("id"));
                    countryCaseDto.setCountryName(rs.getString("country_name"));
                    countryCaseDto.setCases(rs.getInt("cases"));
                    LocalDate date = LocalDate.parse(rs.getString("date"));
                    countryCaseDto.setDate(date);

                    return countryCaseDto;
                });


        return countryCaseDtoList;
    }

    @Override
    public List<CountryCaseDto> getCountryCasesByDates(List<String> countries, LocalDate from, LocalDate to) {
        final String query = "SELECT ctc.id AS id, c.country_name AS country_name, ctc.cases AS cases, ctc.date AS date FROM country_case AS ctc " +
                "INNER JOIN country AS c ON c.id=ctc.country_id " +
                "WHERE c.country_name in (:countryNames) AND date >= :fromDate AND date <= :toDate";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_FORMAT);
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("countryNames", countries);
        params.addValue("fromDate",  formatter.format(from));
        params.addValue("toDate",  formatter.format(to));

        List<CountryCaseDto> countryCaseDtoList = jdbcTemplate.query(query, params,
                (rs, rowNum) -> {
                    CountryCaseDto countryCaseDto = new CountryCaseDto();
                    countryCaseDto.setId(rs.getInt("id"));
                    countryCaseDto.setCountryName(rs.getString("country_name"));
                    countryCaseDto.setCases(rs.getInt("cases"));
                    LocalDate date = LocalDate.parse(rs.getString("date"));
                    countryCaseDto.setDate(date);

                    return countryCaseDto;
                });

        return countryCaseDtoList;
    }
}
