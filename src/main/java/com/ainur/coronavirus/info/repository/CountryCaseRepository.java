package com.ainur.coronavirus.info.repository;

import com.ainur.coronavirus.info.model.dto.CountryCaseDto;
import com.ainur.coronavirus.info.model.dto.CountryDto;

import java.time.LocalDate;
import java.util.List;

public interface CountryCaseRepository {

    int[] saveAll(List<CountryCaseDto> countryCaseDtoList, CountryDto countryDto);

    List<CountryCaseDto> getLastCasesByCountryIds(List<Integer> countryIds);

    List<CountryCaseDto> getCountryCasesByDates(List<String> countries, LocalDate from, LocalDate to);
}
