
### REST API Documentation
http://localhost:8080/swagger-ui-custom.html  

or

http://localhost:8080/swagger-ui/index.html#/


### Database

The application uses in memory database H2.

http://localhost:8080/h2-console
 
username="sa" / password="sa"

### List of available countries

If you want to customize the list of available countries,
then you can change the parameter "coronavirus-info.country.list" in application.properties

If you want to get data from all countries in the world,
then you need to set the "coronavirus-info.country.filter" parameter "false" value


### Country confirmed cases

In the current version of the application, the speed of preparing the application for operation
and downloading data for the database by country depends on the number of countries.
On average, 10 seconds for each country.

By default, new data is updated every 24 hours.